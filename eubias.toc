\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Overview}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{A concrete example}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Structure}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Workflow}{5}{0}{1}
\beamer@sectionintoc {2}{How it works}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Image processing}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Measurements}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{Statistics}{14}{0}{2}
\beamer@sectionintoc {3}{About}{19}{0}{3}
\beamer@subsectionintoc {3}{1}{Tango on the web}{19}{0}{3}
\beamer@subsectionintoc {3}{2}{Tango team}{20}{0}{3}
